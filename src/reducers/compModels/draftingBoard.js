import { DISPLAY_NOTEPAD } from 'PipeRouter';


const initialState = {
  display: 'INDEX'
};

function draftingBoard(state=initialState, action) {
  switch (action.type) {
    case DISPLAY_NOTEPAD:
      return { display: action.data };

    default:
      return state;
  }
}


export default draftingBoard;
