export const AUTOSAVE_TEXT = 'AUTOSAVE_TEXT';
export const autoSaveText = event => {
  return {
    type: AUTOSAVE_TEXT,
    data: event.target.value
  };
};

export const SAVE_NOTE = 'SAVE_NOTE';
export const saveNote = note => {
  return {
    type: SAVE_NOTE,
    data: note
  };
};


const initialState = {
  text: '',
  title: '',
  createDate: '',
  lastModified: ''
};

function note(state=initialState, action) {
  switch (action.type) {
    case SAVE_NOTE:
      return { ...state, ...action.data };

    case AUTOSAVE_TEXT:
      return { ...state, text: action.data }

    default:
      return state;
  }
}


export default note;
