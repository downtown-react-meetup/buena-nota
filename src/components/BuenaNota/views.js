import React from 'react';

import CSS from './styles.css';

import LeftPanel from 'Components/LeftPanel';
import CollectionsPanel from 'Components/CollectionsPanel';
import Header from 'Components/Header';
import DraftingBoard from 'Components/DraftingBoard';

import AddNewFolder from 'Shared/Icons/AddNewFolder';


const BueanNotaViews = () => {
  return (
    <div className={ CSS['container'] }>
      <div className={ CSS['left-block'] }>
        <LeftPanel />
        <CollectionsPanel />
      </div>

      <div className={ CSS['right-block'] }>
        <Header />
        <DraftingBoard />
      </div>

    </div>
  );
};


export default BueanNotaViews;
