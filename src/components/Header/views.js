import React from 'react';

import CSS from './styles.css';

import { Settings } from 'Shared/Icons';


const HeaderViews = () => {
  return (
    <div className={ CSS['container'] }>
      <span className={ CSS['title'] }>Buena Nota</span>
      <Settings className={ CSS['icon-settings'] } />
    </div>
  );
};


export default HeaderViews;
