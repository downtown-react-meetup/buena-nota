import React from 'react';

import CSS from './styles.css';

import ToolChest from './ToolChest';
import DocumentSelection from './DocumentSelection';
import IndexBoard from './IndexBoard';


const DraftingBoardViews = () => {
  return (
    <div className={ CSS['container'] }>
      <ToolChest />
      <DocumentSelection />
      <IndexBoard />
    </div>
  );
};


export default DraftingBoardViews;
