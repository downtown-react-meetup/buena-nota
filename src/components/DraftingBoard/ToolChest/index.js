import React, { Component } from 'react';

import ToolChestViews from './views';


class ToolChest extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <ToolChestViews />;
  }
}


export default ToolChest;
