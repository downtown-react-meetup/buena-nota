import React from 'react';

import CSS from './styles.css';

import { PaperClip, Trash } from 'Shared/Icons';


const ToolChestViews = () => {
  return (
    <section className={ CSS['container'] }>
      <div className={ CSS['left'] }>
        <PaperClip className={ CSS['icon'] } width="14" viewBox="0 0 16 24" />
      </div>
      <div className={ CSS['right'] }>
        <Trash className={ CSS['icon'] } height="28" />
      </div>
    </section>
  );

};


export default ToolChestViews;
