import React, { Component } from 'react';
import { connect } from 'react-redux';

import { autoSaveText } from 'Reducers/dataModels/note';

import IndexBoardViews from './views';


class IndexBoard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <IndexBoardViews { ...this.props } />
  }
}


export default connect(null, {
  autoSaveText
})(IndexBoard);
