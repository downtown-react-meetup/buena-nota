import React from 'react';

import CSS from './styles.css';


const IndexBoardViews = ({ autoSaveText }) => {
  return (
    <section className={ CSS['container'] }>
      <input className={ CSS['title'] } type="text" placeholder="Untitled" />
      <textarea className={ CSS['textarea'] } onChange={ autoSaveText }></textarea>
    </section>
  );
};


export default IndexBoardViews;
