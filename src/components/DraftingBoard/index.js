import React, { Component } from 'react';

import DraftingBoardViews from './views';


class DraftingBoard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <DraftingBoardViews />;
  }
}


export default DraftingBoard;
