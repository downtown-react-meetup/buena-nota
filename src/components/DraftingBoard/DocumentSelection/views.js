import React from 'react';

import CSS from './styles.css';


const DocumentSelectionViews = ({ display, handleSelection, items }) => {
  return (
    <ul className={ CSS['container'] }>
      {
        items.map((item, key) => {
          let css = CSS['item'];

          if (item === display) {
            css = `${CSS['item--selected']}`;
          }

          return (
            <li key={ key } className={ css } onClick={ () => handleSelection(item) }>
              { item.toLowerCase() }
            </li>
          );
        })
      }
    </ul>
  );
};


export default DocumentSelectionViews;
