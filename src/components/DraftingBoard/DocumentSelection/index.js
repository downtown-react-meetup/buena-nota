import React, { Component } from 'react';
import { connect } from 'react-redux';

import DocumentSelectionViews from './views';

import { displayNotePad } from 'PipeRouter';


class DocumentSelection extends Component {
  constructor(props) {
    super(props)
  }

  handleSelection = (notePad) => {
    this.props.displayNotePad(notePad);
  }

  render() {
    const { display, displayNotePad } = this.props;
    const items = ['WEB', 'LETTER', 'NOTE', 'INDEX', 'POSTIT'];

    const viewProps = {
      display,
      handleSelection: this.handleSelection,
      items
    }

    return <DocumentSelectionViews { ...viewProps } />;
  }
}


function mapStateToProps(state) {
  const { display } = state.draftingBoard;

  return {
    display
  };
}

export default connect(mapStateToProps, {
  displayNotePad
})(DocumentSelection);
