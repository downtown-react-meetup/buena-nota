import React from 'react';
import PropTypes from 'prop-types';


const PaperClip = props => {
  let ariaLabelledBy;
  let titleId;
  let descId;

  const {
    hidden, title, desc, role,
    width, height, viewBox,
    fill, className, x, y,
    handleClick
  } = props;

  if (title) {
    titleId = (Math.random() * 100000).toFixed(0);
    ariaLabelledBy = `${titleId}`;
  }

  if (desc) {
    descId = (Math.random() * 100000).toFixed(0);
    ariaLabelledBy += ` ${descId}`;
  }

  return (
    <svg className={ className } onClick={ handleClick }
      width={ width }
      height={ height }
      viewBox={ viewBox }
      fill={ fill }
      role={ role }
      aria-labelledby={ ariaLabelledBy }
      aria-hidden={ hidden }
      x={ x }
      y={ y }>

      {title
        ? <title id={ titleId }>{ title }</title>
        : null
      }

      {desc
        ? <desc id={ descId }>{ desc }</desc>
        : null
      }

      <g id="ProjectNight" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="App" transform="translate(-873.000000, -112.000000)" stroke="#231F20">
          <g id="layout">
            <g id="draftingBoard" transform="translate(378.000000, 78.000000)">
              <g id="tools" transform="translate(436.000000, 30.000000)">
                <polyline id="Paperclip" transform="translate(67.000000, 17.000000) scale(-1, 1) translate(-67.000000, -17.000000) " points="64.6358543 12.4071111 64.6358543 24.5555556 69.3370682 24.5555556 69.3370682 5 60 5 60 29 74 29 74 12.6426667" />
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

PaperClip.defaultProps = {
  className: '',
  width: '24',
  height: '24',
  viewBox: '0 0 24 24',
  x: '',
  y: '',
  fill: '#000000',
  role: 'img',
  title: '',
  desc: '',
  hidden: false
};

PaperClip.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  viewBox: PropTypes.string,
  x: PropTypes.string,
  y: PropTypes.string,
  fill: PropTypes.string,
  role: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  hidden: PropTypes.bool
};


export default PaperClip;
