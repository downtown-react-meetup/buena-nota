const path = require('path');


module.exports = {
  contextPath: path.resolve(__dirname, '..', 'src'),
  contentBasePath: path.resolve(__dirname, '..', 'src'),
  appEntry: './index.js',
  outputPath: path.resolve(__dirname, '..', 'build'),
  cssPaths: [
    path.resolve(__dirname, '..', 'src/assets/styles'),
    path.resolve(__dirname, '..', 'src/components'),
    path.resolve(__dirname, '..', 'src/shared')
  ],
  postcssRootPath: path.resolve(__dirname, '..', 'src'),
  jsPaths: [
    path.resolve(__dirname, '..', 'src/components'),
    path.resolve(__dirname, '..', 'src/configureStore.js'),
    path.resolve(__dirname, '..', 'src/gateway'),
    path.resolve(__dirname, '..', 'src/index.js'),
    path.resolve(__dirname, '..', 'src/lib'),
    path.resolve(__dirname, '..', 'src/reducers'),
    path.resolve(__dirname, '..', 'src/pipeline'),
    path.resolve(__dirname, '..', 'src/piperouter'),
    path.resolve(__dirname, '..', 'src/sagas'),
    path.resolve(__dirname, '..', 'src/services')
  ],
  fontPathEntry: [path.resolve(__dirname, '..', 'src/assets/fonts')],
  fontPathOutput: 'assets/fonts/[name].[ext]',
  recordsPath: path.resolve(__dirname, '..', './recordsPath.json'),
  resolveAliasPaths: {
    Components: path.resolve(__dirname, '..', 'src/components'),
    Gateway: path.resolve(__dirname, '..', 'src/gateway'),
    Lib: path.resolve(__dirname, '..', 'src/lib'),
    Reducers: path.resolve(__dirname, '..', 'src/reducers'),
    Pipeline: path.resolve(__dirname, '..', 'src/pipeline'),
    PipeRouter: path.resolve(__dirname, '..', 'src/piperouter'),
    Services: path.resolve(__dirname, '..', 'src/services'),
    Shared: path.resolve(__dirname, '..', 'src/components/shared'),
    Store: path.resolve(__dirname, '..', 'src/configureStore.js'),
    Styles: path.resolve(__dirname, '..', 'src/assets/styles')
  },
  resolveAliasModules: [
    path.resolve(__dirname, '..', 'src'),
    'node_modules'
  ]
};
