const {
  appEntry,
  contextPath,
  outputPath,
  jsPaths,
  fontPathEntry,
  fontPathOutput,
  resolveAliasPaths,
  resolveAliasModules
} = require('./common-paths');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');


const config = {
  context: contextPath,
  devtool: 'source-map',
  entry: {
    app: [
      'react-hot-loader/patch',
      appEntry
    ]
  },
  output: {
    path: outputPath
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        include: jsPaths
      },
      {
        test: /\.(ttf|otf)$/,
        include: fontPathEntry,
        use: [{
          loader: 'file-loader',
          options: {
            mimetype: 'application/octet-stream',
            name: fontPathOutput
          }
        }]
      }
    ]
  },

  plugins: [
    new webpack.ProgressPlugin(),

    new CaseSensitivePathsPlugin({
      debug: false
    }),

    //see possible syntax errors at the browser console instead of hmre overlay
    new webpack.NoEmitOnErrorsPlugin()
  ],

  resolve: {
    alias: resolveAliasPaths,
    extensions: ['.js', '.jsx'],
    enforceExtension: false,
    modules: resolveAliasModules
  }
};


module.exports = config;
