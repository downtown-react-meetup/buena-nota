const path = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const { cssPaths, postcssRootPath, recordsPath } = require('./common-paths');

module.exports = {
  entry: {
    vendor: [
      'react-hot-loader', 'react', 'react-dom', 'prop-types',
      'react-redux', 'redux-saga', 'moment', 'babel-polyfill', 'styled-components'
    ]
  },

  output: {
    publicPath: './',
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].js'
  },

  recordsPath: recordsPath,

  module: {
    rules: [
      {
        test: /\.css$/,
        include: cssPaths,
        use: ExtractTextPlugin.extract(
          {
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  localIdentName: '[folder]__[local]--[hash:base64:10]',
                  sourceMap: true,
                  importLoaders: 1,
                  import: false,
                  url: true
                }
              },
              {
                loader: 'postcss-loader',
                options: {
                  sourceMap: true,
                  plugins: (loader) => [
                    require('postcss-smart-import')({
                      root: postcssRootPath,
                      path: ['assets', 'components'],
                      skipDuplicates: true
                    }),
                    require('postcss-cssnext')()
                  ]
                }
              }
            ]
          }
        )
      }
    ]
  },

  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),

    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest'],
      minChuncks: Infinity
    }),

    new ExtractTextPlugin({
      filename: 'styles.css',
      disable: false,
      allChunks: true
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),

    new HtmlWebpackPlugin({
      template: 'index.html',
      inject: true,
      hash: true,
      cache: true,
      chunksSortMode: 'dependency',
      showErrors: true
    }),

    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      extractComments: false,
      parallel: {
        cache: true,
        workers: true
      },
      uglifyOptions: {
        ecma: 6,
        mangle: true,
        output: {
          comments: false,
          beautify: false
        },
        compress: {
          warnings: false,
          drop_console: true,
          drop_debugger: true,
          dead_code: true
        },
        warnings: true
      }
    }),

    new webpack.optimize.AggressiveMergingPlugin()
  ]
};
