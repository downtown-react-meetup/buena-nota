const commonConfig = require('./build-utils/webpack.common');
const webpackMerge = require('webpack-merge');


module.exports = (env) => {
  console.log(env);
  const envConfig = require(`./build-utils/webpack.${env.env}.js`);
  console.log(commonConfig);
  console.log(envConfig);

  const merged = webpackMerge(commonConfig, envConfig);
  console.log(merged)
  return merged;
};
